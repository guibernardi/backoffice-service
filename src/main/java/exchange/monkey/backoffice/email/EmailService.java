package exchange.monkey.backoffice.email;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
