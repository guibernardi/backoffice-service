package exchange.monkey.backoffice.pessoa;

public interface PessoaService {
    Pessoa save(Pessoa pessoa);
    Pessoa getById(Long id);
    Pessoa requestConfirmation(Pessoa pessoa);
    Pessoa confirm(Pessoa pessoa);
}
