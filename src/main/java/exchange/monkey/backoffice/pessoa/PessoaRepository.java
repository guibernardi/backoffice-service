package exchange.monkey.backoffice.pessoa;

import org.springframework.data.jpa.repository.JpaRepository;

interface PessoaRepository extends JpaRepository<Pessoa, Long> {
    Pessoa findPessoaByEmail(String email);
}
