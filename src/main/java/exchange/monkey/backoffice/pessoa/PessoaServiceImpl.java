package exchange.monkey.backoffice.pessoa;

import exchange.monkey.backoffice.email.EmailService;
import exchange.monkey.backoffice.integration.AuthorizationClient;
import exchange.monkey.backoffice.integration.CustomerClient;
import exchange.monkey.backoffice.integration.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
class PessoaServiceImpl implements PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AuthorizationClient authorizationClient;

    @Autowired
    private CustomerClient customerClient;

    @Override
    public Pessoa save(Pessoa pessoa) {
        Pessoa pes = pessoaRepository.findPessoaByEmail(pessoa.getEmail());

        if (pes != null) {
            if (pes.getSynchronizedAt() != null) {
                throw new RuntimeException(String.format("Pessoa (email: %s) ja confirmou seu cadastro com a Monkey Exchange.",
                        pes.getEmail()));
            } else {
                throw new RuntimeException(String.format("Pessoa (email: %s) já existente e aguardando confirmação de cadastro. " +
                                "Favor verificar.", pessoa.getEmail()));
            }
        } else {
            pes = pessoaRepository.save(pessoa);
        }

        return pes;
    }

    @Override
    public Pessoa getById(Long id) {
        return pessoaRepository.findById(id).orElse(null);
    }

    //@HystrixCommand(fallbackMethod = "requestConfirmationFallback")
    @Override
    public Pessoa requestConfirmation(Pessoa pessoa) {
        if (pessoa == null) {
            throw new RuntimeException("Pessoa não encontrada.");
        }

        if (pessoa.getSynchronizedAt() != null) {
            throw new RuntimeException(String.format("Pessoa (email: %s) já confirmou seu cadastro com a Monkey Exchange.",
                    pessoa.getEmail()));
        }

        pessoa.setToken(jwtTokenProvider.generateToken(pessoa.getId(), pessoa.getEmail()));

        authorizationClient.saveToken(pessoa.getId(), pessoa.getEmail(), pessoa.getToken());

        if (pessoa.getToken() != null) {
            emailService.sendSimpleMessage(pessoa.getEmail(),
                    "Monkey Exchange - Confirmação de cadastro",
                    String.format("http://localhost:8080/api/authorization/v1/authorization/%s/validate", pessoa.getToken()));
        }

        return pessoaRepository.save(pessoa);
    }

    /*public Pessoa requestConfirmationFallback(Pessoa pessoa) {
        throw new RuntimeException("Ops! Serviço indisponível no momento. Tente novamente mais tarde.");
    }*/

    //@HystrixCommand(fallbackMethod = "confirmFallback")
    @Override
    public Pessoa confirm(Pessoa pessoa) {
        CustomerResponse customerResponse = customerClient.saveCustomer(pessoa);

        if (customerResponse == null) {
            throw new RuntimeException(String.format("Não foi possível sincronizar a pessoa %s com a Monkey Exchange.",
                    pessoa.getEmail()));
        }

        pessoa.setSynchronizedAt(LocalDateTime.now());

        return pessoaRepository.save(pessoa);
    }

    /*public Pessoa confirmFallback(Pessoa pessoa) {
        throw new RuntimeException("Ops! Serviço indisponível no momento. Tente novamente mais tarde.");
    }*/
}

