package exchange.monkey.backoffice.integration;

import exchange.monkey.backoffice.pessoa.Pessoa;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "customer", url = "http://localhost:8080/api/customer/v1/customers", fallback = CustomerClientFallback.class)
public interface CustomerClient {

    @RequestMapping(method = RequestMethod.POST)
    CustomerResponse saveCustomer(@RequestBody Pessoa pessoa);
}
