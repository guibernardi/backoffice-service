package exchange.monkey.backoffice.integration;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AuthorizationClientFallback implements AuthorizationClient {

    @Override
    public AuthorizationResponse saveToken(Long pessoaId, String email, String token) {
        throw new RuntimeException("Ops! Serviço indisponível no momento. Tente novamente mais tarde.");
    }
}
