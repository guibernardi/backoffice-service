package exchange.monkey.backoffice.integration;

import lombok.Data;

@Data
public class AuthorizationResponse {
    private Long pessoaId;
    private String email;
    private String token;
}
