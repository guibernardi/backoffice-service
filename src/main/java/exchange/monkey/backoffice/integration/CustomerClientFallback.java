package exchange.monkey.backoffice.integration;

import exchange.monkey.backoffice.pessoa.Pessoa;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CustomerClientFallback implements CustomerClient {
    @Override
    public CustomerResponse saveCustomer(Pessoa pessoa) {
        throw new RuntimeException("Ops! Serviço indisponível no momento. Tente novamente mais tarde.");
    }
}
