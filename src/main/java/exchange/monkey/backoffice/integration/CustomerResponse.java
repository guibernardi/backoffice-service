package exchange.monkey.backoffice.integration;

import lombok.Data;

@Data
public class CustomerResponse {
    private String nome;
    private String email;
    private String telefone;
}
