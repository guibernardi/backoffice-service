package exchange.monkey.backoffice.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "authorization", url = "http://localhost:8080/api/authorization/v1/authorization", fallback = AuthorizationClientFallback.class) //url = "http://localhost:8082/v1/authorization") //, fallback = AuthorizationClientFallback.class)
public interface AuthorizationClient {

    @RequestMapping(value = {"{pessoaId}/{email}/{token}"}, method = RequestMethod.POST)
    AuthorizationResponse saveToken(@PathVariable("pessoaId") Long pessoaId, @PathVariable("email") String email,
                                    @PathVariable("token") String token);
}
