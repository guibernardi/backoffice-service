package exchange.monkey.backoffice.api.v1;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Data
@EqualsAndHashCode(callSuper = true)
@Relation(value = "pessoa", collectionRelation = "pessoas")
class PessoaResource extends ResourceSupport {

    private String nome;
    private String email;
    private String telefone;
}
