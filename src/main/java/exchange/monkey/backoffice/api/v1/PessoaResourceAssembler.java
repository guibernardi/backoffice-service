package exchange.monkey.backoffice.api.v1;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import exchange.monkey.backoffice.pessoa.Pessoa;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class PessoaResourceAssembler extends ResourceAssemblerSupport<Pessoa, PessoaResource> {

    public PessoaResourceAssembler() {
        super(PessoaRestService.class, PessoaResource.class);
    }

    @Override
    public PessoaResource toResource(Pessoa pessoa) {
        PessoaResource pessoaResource = createResourceWithId(pessoa.getId(), pessoa);
        pessoaResource.setNome(pessoa.getNome());
        pessoaResource.setEmail(pessoa.getEmail());
        pessoaResource.setTelefone(pessoa.getTelefone());

        addLinks(pessoaResource, pessoa.getId());

        return pessoaResource;
    }

    public Pessoa toDomain(PessoaResource pessoaResource) {
        return Pessoa.builder()
                .nome(pessoaResource.getNome())
                .email(pessoaResource.getEmail())
                .telefone(pessoaResource.getTelefone())
                .build();
    }

    private void addLinks(PessoaResource pessoaResource, Long pessoaId) {
        pessoaResource.add(linkTo(methodOn(PessoaRestService.class).requestConfirmation(pessoaId)).withRel("requestConfirmation"));
        pessoaResource.add(linkTo(methodOn(PessoaRestService.class).confirm(pessoaId)).withRel("confirm"));
    }
}
