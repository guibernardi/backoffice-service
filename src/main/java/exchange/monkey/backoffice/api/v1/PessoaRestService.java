package exchange.monkey.backoffice.api.v1;

import exchange.monkey.backoffice.pessoa.PessoaService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/v1/pessoas")
public class PessoaRestService {

    private final PessoaService pessoaService;
    private final PessoaResourceAssembler pessoaResourceAssembler;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public PessoaResource save(@RequestBody PessoaResource pessoaResource) {
        return pessoaResourceAssembler.toResource(pessoaService.save(pessoaResourceAssembler.toDomain(pessoaResource)));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public PessoaResource getById(@PathVariable Long id) {
        return pessoaResourceAssembler.toResource(pessoaService.getById(id));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/{id}/request-confirmation"}, method = RequestMethod.POST)
    public PessoaResource requestConfirmation(@PathVariable Long id) {
        return pessoaResourceAssembler.toResource(pessoaService.requestConfirmation(pessoaService.getById(id)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/{id}/confirm"}, method = RequestMethod.POST)
    public PessoaResource confirm(@PathVariable Long id) {
        return pessoaResourceAssembler.toResource(pessoaService.confirm(pessoaService.getById(id)));
    }
}
